1.- Genere un proyecto nuevo en LPCXpresso.

2.- Declare y sume dos variables N1=2 y N2=3 de tipo int y guarde su resultado en la variable M.

3.- Con la placa conectada al puerto USB genere un proceso de debug. Las placas LPC1769 se encuentran disponibles en el pañol de electrónica.

4.- Al final del código C y utilizando comentarios conteste la siguiente preguntas:
a.- ¿En que posiciones de memoria se asignaron las variables N1, N2 y M? Recuerde que para comentar en C se utiliza //.
b.-¿A que parte del mapa de memoria corresponden estas direcciones(Ej.: Flash, SRAM, ROM, etc)?

5.- Obtenga una captura de pantalla en donde se observe el workspace, los comentarios de las respuestas, los valores finales de cada variable y la posición de memoria en donde han sido guardados estos valores LPCXpresso. Nombre del archivo 