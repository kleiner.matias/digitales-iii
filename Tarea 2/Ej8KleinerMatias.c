/*
===============================================================================
 consigna :
 1.- Teniendo un pulsador y seis leds en fila conectados al puerto GPIO de los cuales solo uno se encuentra encendido,
 realizar un programa en C que permita apagar el led encendido y prender el siguiente, tanto cuando se pulsa como cuando
  se suelta el pulsador. Es decir que si pulso y suelto el pulsador la luz del led debería haber avanzado dos posiciones.
   Al llegar al final de la fila la luz debe seguir desde el inicio nuevamente. Pruebe el algoritmo realizado con y sin el
   anti-rebote propuesto en el ejercicio anterior. Evalúe con que retardo es mas conveniente utilizar la función de retardo
   que debe agregarse al algoritmo de anti-rebote. Adjuntar el código realizado. Nombre del archivo Ej8ApellidoDelEstudiante.c

2.- Adjuntar un video mostrando el funcionamiento obtenido con el diseño que tiene implementado el anti-rebote. Nombre del archivo Ej8ApellidoDelEstudiante.
===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define AddrFIO0DIR 0x2009C000 // Define las posiciones de memoria
#define AddrFIO0SET 0x2009C018 // donde se encuentran los registros
#define AddrFIO0CLR 0x2009C01C // que configuran al GPIOO
#define AddrFIO0MASK 0x2009C010
#define AddrFIO0PIN 0x2009C014
#define maximo 350000
#define minimo 0

unsigned int volatile * const FIO0DIR = (unsigned int *) AddrFIO0DIR; // Define los punteros a las direcciones
unsigned int volatile * const FIO0SET = (unsigned int *) AddrFIO0SET; // de memoria definidas por las
unsigned int volatile * const FIO0CLR = (unsigned int *) AddrFIO0CLR; // correspondientes constantes.
unsigned int volatile * const FIO0PIN = (unsigned int *) AddrFIO0PIN;
unsigned int volatile * const FIO0MASK = (unsigned int *) AddrFIO0MASK;


static int SampleA;	//Muestra en T=0

int debounce (int);

int main(void) {

	int variable=0;		//Contador que me permite pasar de estado en estado
	int estado1;
	int estado2;
	*FIO0DIR &= (0<<1); //define al pin 1 del puerto O como entrada
	*FIO0DIR |= (0x3F << 6); //define al pin 6 al 11 del Puerto 0 como de salida (en "1").


	int e1=0x1; 	//Estado1 led 1 prendido
	int e2=0x2;		//Estado2 led 2 prendido
	int e3=0x4;		//Estado3 led 3 prendido
	int e4=0x8;		//Estado4 led 4 prendido
	int e5=0x10;	//Estado5 led 5 prendido
	int e6=0x20;	//Estado6 led 6 prendido

	while (1) {

		estado1=debounce((*FIO0PIN & 0b10)==0);
		for (unsigned int i=0; i <1000 ; i ++);		 //Delay
		estado2=debounce((*FIO0PIN & 0b10)==0);
		//  SI SE COMENTA LAS TRES LINEAS DE ARRIBA, Y SE DESCOMENTA LAS TRES DE ABAJO, EL CIRCUITO VA A FUNCIONAR SIN ANTIRREBOTE
		//		estado1=((*FIO0PIN & 0b10)==0);
		//		for (unsigned int i=0; i <1000 ; i ++) {}
		//		estado2=((*FIO0PIN & 0b10)==0);

		//ingresa si realmente esta pulsado.
		if((estado1 != estado2)){

			if (variable==0) {						//Estado1
				*FIO0PIN=(e1<<6);
				variable=variable+1;
			}
			else if (variable==1) {					//Estado2
				*FIO0PIN=(e2<<6);
				variable=variable+1;
			}
			else if (variable==2) {					//Estado3
				*FIO0PIN=(e3<<6);
				variable=variable+1;
			}
			else if (variable==3) {					//Estado4
				*FIO0PIN=(e4<<6);
				variable=variable+1;
			}
			else if (variable==4) {					//Estado5
				*FIO0PIN=(e5<<6);
				variable=variable+1;
			}

			else if (variable==5) {					//Estado6
				*FIO0PIN=(e6<<6);
				variable=0;
			}


		}
	}
	return 0;
}
int debounce(int SampleA)			//ANTIREBOTE
{
	static int SampleB = 0;
	static int SampleC = 0;
	static int LastDebounceResult = 0;

	LastDebounceResult = LastDebounceResult & (SampleA | SampleB | SampleC) | (SampleA & SampleB & SampleC);
	SampleC = SampleB;
	SampleB = SampleA;
	return LastDebounceResult;
}
